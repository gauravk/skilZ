class SkilzusersController < ApplicationController
  before_filter :authenticate_skilzuser!
  
  # GET /skilzusers
  # GET /skilzusers.json
  def index
    @skilzusers = Skilzuser.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @skilzusers }
    end
  end

  # GET /skilzusers/1
  # GET /skilzusers/1.json
  def show
    @skilzuser = Skilzuser.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @skilzuser }
    end
  end

  # GET /skilzusers/new
  # GET /skilzusers/new.json
  def new
    @skilzuser = Skilzuser.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @skilzuser }
    end
  end

  # GET /skilzusers/1/edit
  def edit
    @skilzuser = Skilzuser.find(params[:id])
  end

  # POST /skilzusers
  # POST /skilzusers.json
  def create
    @skilzuser = Skilzuser.new(params[:skilzuser])

    respond_to do |format|
      if @skilzuser.save
        format.html { redirect_to @skilzuser, notice: 'Skilzuser was successfully created.' }
        format.json { render json: @skilzuser, status: :created, location: @skilzuser }
      else
        format.html { render action: "new" }
        format.json { render json: @skilzuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /skilzusers/1
  # PUT /skilzusers/1.json
  def update
    @skilzuser = Skilzuser.find(params[:id])

    respond_to do |format|
      if @skilzuser.update_attributes(params[:skilzuser])
        format.html { redirect_to @skilzuser, notice: 'Skilzuser was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @skilzuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /skilzusers/1
  # DELETE /skilzusers/1.json
  def destroy
    @skilzuser = Skilzuser.find(params[:id])
    @skilzuser.destroy

    respond_to do |format|
      format.html { redirect_to skilzusers_url }
      format.json { head :no_content }
    end
  end
end
