class CreateSkilzusers < ActiveRecord::Migration
  def change
    create_table :skilzusers do |t|
      t.string :coreid
      t.integer :accountType

      t.timestamps
    end
  end
end
