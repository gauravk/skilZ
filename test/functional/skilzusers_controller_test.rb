require 'test_helper'

class SkilzusersControllerTest < ActionController::TestCase
  setup do
    @skilzuser = skilzusers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:skilzusers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create skilzuser" do
    assert_difference('Skilzuser.count') do
      post :create, skilzuser: { accountType: @skilzuser.accountType, coreid: @skilzuser.coreid }
    end

    assert_redirected_to skilzuser_path(assigns(:skilzuser))
  end

  test "should show skilzuser" do
    get :show, id: @skilzuser
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @skilzuser
    assert_response :success
  end

  test "should update skilzuser" do
    put :update, id: @skilzuser, skilzuser: { accountType: @skilzuser.accountType, coreid: @skilzuser.coreid }
    assert_redirected_to skilzuser_path(assigns(:skilzuser))
  end

  test "should destroy skilzuser" do
    assert_difference('Skilzuser.count', -1) do
      delete :destroy, id: @skilzuser
    end

    assert_redirected_to skilzusers_path
  end
end
